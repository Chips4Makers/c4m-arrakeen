# SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
import os
from textwrap import dedent
from pathlib import Path
from shutil import rmtree, copytree, ignore_patterns
from xml.etree import ElementTree as ET
from typing import Tuple, Iterable

from doit.tools import create_folder

from .paths import *
from .version import get_version, version_file

__all__ = ["task_salt", "task_copy_salt", "task_reset_salt"]


# Globals


_grain_icon = (
    "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAACXBIWXMAAAX/AAAF/wHJdq1WAAAAGXRFWHRTb2Z0d2Fy"
    "ZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAEkRJREFUeJzlW2tsHNd1/u6d587uzOyTy10uKZKiXjFlWVLSSI7S+hEJkdKo"
    "QBO1CRo3gPvHceUi7g+3EYpCzY/+SgCnAQLECOACNRpAKVS0dl3IMiRZsdPSsmSHlEyJsiiK4lN87Htmdx739gc5K0qW"
    "AkMcOgVygcFyhoM73/nuOeeee+65wO94I5/2B1988cVsPB4vaJrWLkmSwRirU0pnGo3G9PHjx6d+8Ytf+J8mnjUn4Lvf"
    "/W5u48aNhwuFwpcVRdmkKEpUEAQwxqBpGur1OhRFAWMMrus2G43GtWazeXp6evonzzzzzIdrjW/NCDh06JCwZcuWH/f1"
    "9f1FJpORNU2DLMuo1WowDAODg4PYv38/zp49iw0bNmB6ehqyLCOZTMK2bViWxW7duvXa9evXnzp69GhlrXCKa9VxMpn8"
    "oWma34nH45AkCbIsg3OO2dlZbNmyBZs2bYKmafj6178Oy7Kgqip83wchBL7vw3VdmkgkDs7Pz/8bgH1rhXPNCHAc54vV"
    "ahWVSgWyLMNxHKiqihs3bmBubg6MMViWBQDwPA/FYhHnz5/Hnj174DgOPM9DpVJBqVT67FphBNaQgHg8TkzTxLp16zA9"
    "PQ3XdeE4Dvbs2QPbtnH8+HGsW7cOpmnil7/8Jb70pS9hx44dKJVKEAQBCwsLuHz5MhRF4WuFEVhDAizLcnp7e3Hq1Clo"
    "mobJyUm0tbUhmUzCMAx0d3dDEATU63Vs27YNk5OTyOVyiMVi+PDDD3H58mX09vaiXq831wojsIYEGIYhDQ0N4eDBg7Bt"
    "G6dOncLCwgImJyfheR4YY613CSEQBAGRSAS9vb3YunUrOjs78f7770PTNHmtMAJrNAscPnw4LwjC4IEDB1K3bt3CqVOn"
    "EI1GsWHDBhQKBTSbzRYJgfCqqiKbzeLy5ct4//33YZomtm3bhtHRUV4oFI6//fbbz7300kvTYWMNnYDvfe97X+jv739t"
    "eHg4bhgGVFXFnj17MDg4iJmZGdy4cQOzs7OwbRu2bUNVVciyjEwmg927d2Pnzp2glOLixYu4du0adF2H4zjo7e0tDgwM"
    "fOUHP/jB/4SJN3QT6OzsfLlarcbL5TLa29uxd+9evPbaaxgYGAClFKlUCg8//DDi8ThkWYbv+7AsC4uLi7h48SLOnDmD"
    "jRs3YuvWrchkMnjrrbfQ09ODcrmc6Ojo+GcAm8LEGzoBruu2ZbNZ5HI5nD9/Hq+++iq2b9+Offv2IRaLwbIs1Ot11Ot1"
    "LCwsQNM0qKqKDRs24KGHHoJhGDh79ixefvllEEKQy+UQj8dhmiauXLmSChtv6AQMDQ29E4lEDmzcuBHxeBwTExOYmprC"
    "Bx98gFu3bsF13VZgRCkF5xyu68J1XRBCkEgkkEwmkU6n0dHRga6uLsiyjDfffBO2bYeq/sAa+IDh4eFTJ0+e3HPu3Dlx"
    "w4YNpKurC6ZpYn5+HqVSCZVKBfV6veUIAUAURaiqimg0CsMw0N7ejkKhgPHxcbzzzjs4f/48f/rpp90nn3zyrU2bNoUa"
    "FYZKwKFDh4Tvf//70+l0Wq3Vavorr7yC06dPY2FhgTPGiGEYyGQyME0T0WgUkiQBABzHgeu6oJSCUopyuYyTJ0+is7MT"
    "+/btwze+8Q1ks9lqvV5vHDlyJBfmijFUAl599dW/2rVr1z+m0+koANRqNZTLZbiuaw0MDGiXLl3C5OQkZmdnUavV4DgO"
    "AECWZei6jmw2i3w+j/7+fvT393NFUUg6nYZpmgCA+fn56rlz5/7+wIEDL4aFOVQfwBjbvLi46CaTSVBKEYvFEI1GHcdx"
    "vL179+Lxxx+H7/vwPK/122zeDvQEQYAsy5BlGaZpWrFYTCWECMt9Y35+3vN9f2uYmEMlQNf1dalUKj45OTlRKBQKhBCH"
    "ENJQFMVQFOVj73ueh1KpBFmWIUkSBEFY+e+o7/tVURRVzrk0MTExYZpmIZlMrgsTc6gEqKrazjlHPB4v1Gq1ac65Fo1G"
    "zbsEu/1xUYRhGPftz/d9nXO+4DiOI4pigTEGRVFyYWIOlQBFUdKccxBCYNu2yhgzqtVqgxBSo5TalFImSZJEKdVjsZge"
    "ELM8FTYcxykxxmqu63Lf9xXf9xMAUul0uhLkCURRTISJOVQCJEnSGWOglIIx5hFCoCiKIgiCQimFIAitub9SqdjNZrPm"
    "eR4kSYqJohhhjLUzxsD50gqYEILle0YIAecclNJomJhpiH0RQRAihBAsXx6wJMTKX2BJ9WOxmKrrelqW5Ywsy5E7Olru"
    "owWSUj94JkmS+vzzz9/x/mpaaAQcPXp0vaZpcgDU8zz/XsLf69n92goiWHBvGIaUSqV2hoU7NAIymcyjsViMUko/Bjpo"
    "dwt/90jfr3HOefBuJBIhmUxmR1i4Q/MB0Wi0WxTF1ho/+A1a03EhSYCqCChV6jg/Mg4joqKn3YQkSb+RCEopX0lWKpUK"
    "bSoMjQBd19uBpVGllML3l6JVz/fxxq8uEtvx4fkeNhXS/MrNOdKe78DcTBkDQyN4bOdmdOdSv1Ejgue+7yORSPSFhTs0"
    "AiKRSCdwp1p7vo/jpz8gmfYcVEkBOMfVmRkiKBqMWAygIqoOx9uDowCAruzSDHc/IoKpMBKJFMLCHZoPiMVi+UDtly/6"
    "1oUR4so6RhcauDZTxnTRgmZmIEsSro2No2o1wTmHEovjzIUrH+sz6Mv3fRr8vRwMpcPCHRoBiqKkgxiAEAKr0RRminU4"
    "XALnSx9quB5mijXYkBGNRsFdG7JIAUIgCCLc5eXx3W3JB7bIgCiKZli4QyNAkiRzpQZU601ZkBRwcERFD3JzHlJjAarI"
    "YTUczFs+fCoD4JDAoIoE9D72zzkXVpqFLMuRsGKBUHzACy+8oCuKogVhMACk41FIbAJRcAj2DJ7ob6LZcHH6chnReAEe"
    "UVCsWqDMRbO6CFWW8a8n3wM4xxcf7kE+c0fES1dOnaZpStlsdieAt1eLPRQC0un0jng8Lq0cJcaY+IX+Tn71+hB27UiQ"
    "TPtmAC6y2Y9w8sJNOCQPSRRRLpaRaGsHoRJ8n0GiHGc/+Aj7f28jIpFIMKO04osgFkgkEjvw/4WARCKxfeXm5rIpyKI/"
    "g8+vb5K0XgOxzoFIJtLpLL72ZAeujF7Hr2/q2L7rEcxVHcwsViFQCs4YqChh6uYFZLUxCBQgbZ9X4tnP3zE7JBKJUGaC"
    "UAjQdT0fzP/LCyEoiiJJjSmSSadBiQsmtAEQIfIqFFHBtv7tmHcW0PQBCo4I8UBEAbUGwEQV12Yr2PlEPyghWJgZlPz4"
    "ehBye+msaVooBITiBCORSC4YnWUCmKZpghxJg0hJECkLQY7D5RH4QgeIoMNrlpA2VfiNOihzkDBjoIKMWqMJIkgoNQ04"
    "TRsEPhLtnyGVqf/iwO1YwDCMzjCwr1oDOOfk9ddfbwuEJ4TAdV1fURRRMPtR96p4Z/Amfv3RR/C5CJcxPNq/Hv0b1yOe"
    "oIiaBKVaE7dKdfjcgq6pcO0aVNmGJHcBFBBZCZR4AG6H2ZFIpD0E+VdHAOecAKCRSCQLLOX0ljWBUUpJxY/iRz8/CzOV"
    "Bte7IBAOkTOcHboJPWagr6cbrOEgoohImxEIvg2vUkQM09j12TgkOQLeHAOcm5ClOJhXASF6EAskQ5A/FBOgmqZllpMV"
    "WGkKZy9cRnfvBnx1zw4wAgAEICJ0I4nBq+NYXFiAIonQIzI06qInUcH+HTK++lg/skkJxBoAaY6Bg8J1HQiCtjIajIUR"
    "C6zWBMjExISoKIoRELCSBIlwVGp1/Pf/DkNwHSiyCCprqPk+Jmduwp14A6UpAsV4CJ1dTyCqbQZ3F8FqF8DceYDoqDkR"
    "lCp1RHXTESRN8ZdT6fF4PJRYYLUaQF5//fU+wzA0AKB0qbti2SI/+vkbuHjjFhy7AcJ9aNEYIGmYKdZgOQwNFyh0fwZ9"
    "Wz4HwZ8Ca86BEAp4c/BgolyTMTdfQqlURtXrQarnT+sA7hULrKqt2gkqirJb13UagAOAfz97gZptHTCIgLmyhdmaDbdq"
    "A5yDEo6oLADwQEkMAEGybRPmJk7DiBdAAHAiIRaNwTCTsOsl2DNzEATB9zyvRUCz2UR3d/deAP+0Gvyr9gGapnUFCY2A"
    "gGLFIpIk4/psBXNlG1QgiIocqluBUJlCc24E+/vnEI3nQQQDmtoEd6bB/AYADpkXIfI5ABJsuwk5krsjKyQIAmzbRjab"
    "feLYsWOr8gOr9gHpdPqPLMtCNHo7WUsAjC/UYTs+ZIkCpZtISdexPlNHVy6Grs5uxJObQLxZwLkOMB+EiHBcDwqJAIIG"
    "F0kszIyi7sag53aCEMID59psNuG6LmKxmGaa5j9wzv+GEPJAxVSrJUCLRqM9juMgFou1NEBVRL7QbAAEUFgD3cZV/Pkf"
    "74coyUDjI8D+ECi/B84YAAKP5iDKJjg1UK7Mwq7V0GxwIPYHSMSyrexSkGmybRvA0p5iLpf7JiHkhQcVYFUmcOLEiZ5E"
    "IqERQlobnQCw5+H1vuTZIASghCFjEIiSAZRPA9ZFcMho+hkUrRSmy20Yn5hDqucQJCUONbEd0exemIX9iBq51h4B55ww"
    "xmDbNgghEEURgiAgk8l0nDhx4g9/KwQ0m81nEokEEQQBnue1SHhofcEzJR9JVOHUyijbALd+DTSvo1SPYGp6DtVaHXXb"
    "xaKVQHrLESh6LzjnKwW+41uMMVKtVsE5hyiKkCQJlFLIskwSicQDa8CqTCCRSHxhebMCAGBZVlD3w//yT57E5MwcrFoF"
    "7uwwnMoVKLyJuFJEXPKAxGOoF4dgLUrQorGW8EFOYcVOUECGCCxtqqwMtparSj732GOPiWfOnLl3Suk3tFVpgK7rHQsL"
    "C60tL0IIarUafN+XOOco5NrQlklDMftRtxlAFMCrAKBw6h9hdmYCqfwuAEsjHmSUlkcWQZWZLMuwLMvUdZ1JktQqsZEk"
    "CeVyGY7jqM8+++y3H0SGVWmAZVknKpXKN7PZbGv0lgsgFcdxIAgCDMMA7fp9LFy9hEpFAaER+MyDzyeQXP8sND3fKpoU"
    "RRGu64IxBt/30Wg0ACytMTjnVFEUy/d9beXoF4tFmKbptbe3/+eDyHDvfetP2CilH27evPk77e3tJLBJz/OYJElkpUpL"
    "kgwj+yik+C7Iid2IZp9AouNxyKrBXdclQaHEygLKlT5AEITA5i1KqRo4QM/zcOPGDfi+/+bu3bt/+kAyrIaAn/3sZ8PX"
    "rl27ODo62lJJAF4QEnue16oA8zwPVJAgSkuFEkFdkOu68H2/pQUrd4ZXri0kSYLneYooiq1nly5dwrp169zBwcF/eVAZ"
    "Vh0JvvHGGz+vVqusWq0GyRB+96ZGYBrBtdLeA4EDoQKhV16B03NdV6WUOgAwNzcHVVVx7dq1kfHx8WMPin/VBNTr9R9f"
    "uHDh6tWrVwNhSUDASvD32PL+2CbpyhqC4AruGWMQRRGc84bv+xgZGUFHR4c7Ojp69ujRo87HkX2yFoYG1N99992fuq7L"
    "isUiBEEgd6vv/a6ArHuN+t0aANx2kiMjI+jr68PQ0NBwJBL529XgDyUneOzYsRdHRkYGR0ZGQJaruj6JYJ/0vYAEQRCw"
    "uLioS5LETdN0xsbGXvrWt761qvNEYe0M8TNnzvydKIo+Y+yOUb2fSt+dPfok7/q+j9HRUdLT02NdunRp4Omnn/7JaoGv"
    "ahpc2QYHB68ePHjwgCiKhfHxcdi23ToHcC/1DyI83/db9QF3m4xlWZidncXY2Bhu3ryJWq2GZDIJ3/cn33vvvecfeeSR"
    "66vFHRoBAMA5P6fr+tdSqVRR13WvXC6L169fFyYnJ2FZFgghUFW1JWgwG8jy0qGQer2OmZkZjI6OYmxsDLVaDbIsI5VK"
    "WfF4fMyyrFtTU1Pu8PDwO0899dQPw8AcerH0wYMH/1rX9T/L5/P9+Xxezufz9c2bN98yTTO2fI5AYowhkUggnU7zmZkZ"
    "Uq1Wg/ofJBIJZhhGudFozM/PzwulUqm9WCxqpVIJpVKpzBgbFgTh24cPHx4JA+9aHpyM2Lb9ZUEQDuTz+S/m8/m+fD4v"
    "5HK5el9f30I0Go02Gg1NFEXW1tZWbTabtWKxqBeLxbZSqURKpRIrFos3qtXqB5zzX4mieOK5554bChvnp3Z2eP/+/Yai"
    "KF/Rdf0ruVzu0Y6Oju5cLke2bduG5dG1i8Xi1XK5/K5lWQO2bf/HkSNH5tYa16d+eDpohw4d6vI872uHDh2SFhcX3zx8"
    "+PCF3xaW3+n2f9xBI6bLMrUvAAAAAElFTkSuQmCC"
)
_version = get_version()
_salt_submoddir = top_dir.joinpath("Salt")


# Support classes & functions


def write_saltreadme(salt_dir: Path):
    with salt_dir.joinpath("README.md").open("w") as f:
        f.write(dedent("""
            All files from this KLayout salt package are autogenerated from project Arrakeen.
            See [project repo](https://gitlab.com/Chips4Makers/c4m-arrakeen) for more information.
        """[1:]))


class _SaltGrainTree(ET.ElementTree):
    def __init__(self, *,
        name: str,  title: str, doc: str, deps: Iterable[Tuple[str, str]]=(),
    ) -> None:
        grain = ET.Element("salt-grain")
        ET.SubElement(grain, "name").text = name
        ET.SubElement(grain, "token")
        ET.SubElement(grain, "hidden").text = "false"
        ET.SubElement(grain, "version").text = _version
        ET.SubElement(grain, "api-version").text = "0.27; python 3.8"
        ET.SubElement(grain, "title").text = title
        ET.SubElement(grain, "doc").text = doc
        ET.SubElement(grain, "doc-url").text = "doc/readme.html"
        ET.SubElement(grain, "url")
        ET.SubElement(grain, "license").text = "AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0"
        ET.SubElement(grain, "author").text = "Chips4Makers contributors"
        ET.SubElement(grain, "author-contact").text = "https://matrix.to/#/#Chips4Makers_community:gitter.im"
        ET.SubElement(grain, "authored-time")
        ET.SubElement(grain, "installed-time")
        ET.SubElement(grain, "icon").text = _grain_icon
        ET.SubElement(grain, "screenshot")
        for pkg, v in deps:
            dep = ET.SubElement(grain, "depends")
            ET.SubElement(dep, "name").text = pkg
            ET.SubElement(dep, "url")
            ET.SubElement(dep, "version").text = v

        super().__init__(grain)


class _READMETree(ET.ElementTree):
    def __init__(self, *,
        header: str, desc: str,
    ) -> None:
        html = ET.Element("html")
        body = ET.SubElement(html, "body")
        ET.SubElement(body, "h1").text = header
        ET.SubElement(body, "p").text = desc
        p = ET.SubElement(body, "p")
        p.text = dedent("""
            Documentation is under construction but some more information can be found at
        """)
        a = ET.SubElement(p, "a")
        a.attrib["href"] = "https://gitlab.com/Chips4Makers/c4m-arrakeen"
        a.text = "project Arrakeen"
        a.tail = ".\n"
        p = ET.SubElement(body, "p")
        p.text = dedent("""
            This is alpha version and not meant for production use. Future versions will contain
            backward incompatible changes.
        """)
        super().__init__(html)


class _PrimLibMacroTree(ET.ElementTree):
    def __init__(self, *,
        techname: str, modname: str,
    ) -> None:
        macro = ET.Element("klayout-macro")
        ET.SubElement(macro, "description").text = f"{techname} primitives library macro"
        ET.SubElement(macro, "version")
        ET.SubElement(macro, "category").text = "pymacros"
        ET.SubElement(macro, "prolog")
        ET.SubElement(macro, "epilog")
        ET.SubElement(macro, "doc")
        ET.SubElement(macro, "autorun").text = "true"
        ET.SubElement(macro, "autorun-early").text = "false"
        ET.SubElement(macro, "priority").text = "0"
        ET.SubElement(macro, "shortcut")
        ET.SubElement(macro, "show-in-manu").text = "false"
        ET.SubElement(macro, "group-name")
        ET.SubElement(macro, "menu-path")
        ET.SubElement(macro, "interpreter").text = "python"
        ET.SubElement(macro, "dsl-interpreter-name")
        ET.SubElement(macro, "text").text = dedent(f"""
            import pya
            from c4m.pdk import {modname}
            {modname}.pya_register_primlib()
        """[1:])
        super().__init__(macro)


# PDKMaster salt package subtask


_pdkmaster_pyspicemoddir = pdkmaster_moddir.joinpath("deps", "PySpice")
_pdkmaster_saltdir = _salt_submoddir.joinpath("PDKMaster")
_pdkmaster_saltpythondir = _pdkmaster_saltdir.joinpath("python")
_pdkmaster_grainfile = _pdkmaster_saltdir.joinpath("grain.xml")
_pdkmaster_saltdocdir = _pdkmaster_saltdir.joinpath("doc")
_pdkmaster_readmefile = _pdkmaster_saltdocdir.joinpath("readme.html")
_deps = (
    __file__, version_file,
    *pdkmaster_moddir.joinpath("pdkmaster").rglob("*.py"),
    *io_klayout_moddir.joinpath("pdkmaster").rglob("*.py"),
)
def _pdkmaster():
    if _pdkmaster_saltpythondir.exists():
        rmtree(_pdkmaster_saltpythondir)
    if _pdkmaster_saltdocdir.exists():
        rmtree(_pdkmaster_saltdocdir)

    write_saltreadme(salt_dir=_pdkmaster_saltdir)

    copytree(
        pdkmaster_moddir.joinpath("pdkmaster"),
        _pdkmaster_saltpythondir.joinpath("pdkmaster"),
    )
    copytree(
        _pdkmaster_pyspicemoddir.joinpath("PySpice"),
        _pdkmaster_saltpythondir.joinpath("PySpice"),
    )
    copytree(
        io_klayout_moddir.joinpath("pdkmaster", "io", "klayout"),
        _pdkmaster_saltpythondir.joinpath("pdkmaster", "io", "klayout"),
    )
    for d in _pdkmaster_saltpythondir.joinpath("pdkmaster").rglob("__pycache__"):
        rmtree(d)

    grain_tree = _SaltGrainTree(
        name="PDKMaster", title="The PDKMaster framework base library",
        doc=dedent("""
            PDKMaster packaged for KLayout as python library. It includes main repo and all the pdkmaster-io-* modules.
            This is alpha version and not meant for production use.
        """[1:]),
    )
    grain_tree.write(_pdkmaster_grainfile, encoding="unicode", xml_declaration=True)

    create_folder(_pdkmaster_saltdocdir)
    readme_tree = _READMETree(
        header="PDKMaster",
        desc=dedent("""
            This KLayout package contains the base PDKMaster python modules. This will be used by other
            (Chips4Makers) packages.
        """),
    )
    readme_tree.write(_pdkmaster_readmefile, encoding="unicode", method="html")

_pdkmaster_subtask = {
    "name": "pdkmaster",
    "doc": "Generate pdkmaster salt package",
    "file_dep": _deps,
    "targets": (
        _pdkmaster_saltpythondir,
        _pdkmaster_grainfile,
        _pdkmaster_readmefile,
    ),
    "actions": (
        _pdkmaster,
    )
}


# C4M/FlexLibs salt package subtask


_flexlibs_saltdir = _salt_submoddir.joinpath("C4M_FlexLibs")
_flexlibs_saltpythondir = _flexlibs_saltdir.joinpath("python")
_flexlibs_grainfile = _flexlibs_saltdir.joinpath("grain.xml")
_flexlibs_saltdocdir = _flexlibs_saltdir.joinpath("doc")
_flexlibs_readmefile = _flexlibs_saltdocdir.joinpath("readme.html")
_deps = (
    __file__, version_file,
    *flexcell_moddir.joinpath("c4m").rglob("*.py"),
    *flexio_moddir.joinpath("c4m").rglob("*.py"),
)
def _flexlibs():
    if _flexlibs_saltpythondir.exists():
        rmtree(_flexlibs_saltpythondir)
    if _flexlibs_saltdocdir.exists():
        rmtree(_flexlibs_saltdocdir)
        
    write_saltreadme(salt_dir=_flexlibs_saltdir)

    copytree(
        flexcell_moddir.joinpath("c4m", "flexcell"),
        _flexlibs_saltpythondir.joinpath("c4m", "flexcell"),
    )
    copytree(
        flexio_moddir.joinpath("c4m", "flexio"),
        _flexlibs_saltpythondir.joinpath("c4m", "flexio"),
    )
    for d in _flexlibs_saltpythondir.joinpath("c4m").rglob("__pycache__"):
        rmtree(d)

    grain_tree = _SaltGrainTree(
        name="C4M/FlexLibs", title="The Chips4Makers scalable cell library python modules",
        doc=dedent("""
            Chips4Makers scalable cell library packaged for KLayout as python library.
            This is alpha version and not meant for production use.
        """[1:]),
        deps=(("PDKMaster", _version),),
    )
    grain_tree.write(_flexlibs_grainfile, encoding="unicode", xml_declaration=True)

    create_folder(_flexlibs_saltdocdir)
    readme_tree = _READMETree(
        header="Chips4Makers FlexLibs",
        desc=dedent("""
            This KLayout package contains the Chips4Makers scalable cell library generation python code.
            This will be used by the (Chips4Makers) PDK packages to generate their libraries.
        """),
    )
    readme_tree.write(_flexlibs_readmefile, encoding="unicode", method="html")

_flexlibs_subtask = {
    "name": "flexlibs",
    "doc": "Generate Chips4Makers FlexLibs salt package",
    "file_dep": _deps,
    "targets": (
        _flexlibs_saltpythondir,
        _flexlibs_grainfile,
        _flexlibs_readmefile,
    ),
    "actions": (
        _flexlibs,
    )
}


# C4M/PDK/Sky130 salt package subtask


_sky130_saltdir = _salt_submoddir.joinpath("C4M_PDK_Sky130")
_sky130_saltpythondir = _sky130_saltdir.joinpath("python")
_sky130_grainfile = _sky130_saltdir.joinpath("grain.xml")
_sky130_saltdocdir = _sky130_saltdir.joinpath("doc")
_sky130_readmefile = _sky130_saltdocdir.joinpath("readme.html")
_sky130_saltmacrodir = _sky130_saltdir.joinpath("pymacros")
_sky130_macrofile = _sky130_saltmacrodir.joinpath("RegisterPrimLib.lym")
_sky130_salttechdir = _sky130_saltdir.joinpath("tech")
_deps = (
    __file__, version_file,
    *sky130_moddir.joinpath("deps", "pdkmaster-io-klayout", "c4m").rglob("*.py"),
    *sky130_moddir.joinpath("c4m").rglob("*.py"),
)
def _sky130():
    ret = os.system(f"cd {sky130_moddir}; pdm doit klayout")
    assert ret == 0

    if _sky130_saltpythondir.exists():
        rmtree(_sky130_saltpythondir)
    if _sky130_saltdocdir.exists():
        rmtree(_sky130_saltdocdir)
    if _sky130_saltmacrodir.exists():
        rmtree(_sky130_saltmacrodir)
    if _sky130_salttechdir.exists():
        rmtree(_sky130_salttechdir)

    write_saltreadme(salt_dir=_sky130_saltdir)

    copytree(
        sky130_moddir.joinpath("c4m", "pdk", "sky130"),
        _sky130_saltpythondir.joinpath("c4m", "pdk", "sky130"),
    )
    for d in _sky130_saltpythondir.joinpath("c4m").rglob("__pycache__"):
        rmtree(d)
    copytree(
        sky130_moddir.joinpath("open_pdk", "C4M.Sky130", "libs.tech", "klayout", "tech", "C4M.Sky130"),
        _sky130_salttechdir,
    )

    grain_tree = _SaltGrainTree(
        name="C4M/PDK/Sky130", title="The Chips4Makers PDKMaster based PDK for Sky130",
        doc=dedent("""
            This is Chips4Makers PDK version of the Sky130 PDK using the project Arrakeen framework.
        """[1:]),
        deps=(
            ("PDKMaster", _version),
            ("C4M/FlexLibs", _version),
        ),
    )
    grain_tree.write(_sky130_grainfile, encoding="unicode", xml_declaration=True)

    create_folder(_sky130_saltdocdir)
    readme_tree = _READMETree(
        header="Chips4Makers Sky130 PDK",
        desc=dedent("""
            This KLayout package contains the Chips4Makers PDK version of the Sky130 PDK using the project Arrakeen
            framework.
        """),
    )
    readme_tree.write(_sky130_readmefile, encoding="unicode", method="html")

    create_folder(_sky130_saltmacrodir)
    macro_tree = _PrimLibMacroTree(techname="C4M.Sky130", modname="sky130")
    macro_tree.write(_sky130_macrofile, encoding="unicode", xml_declaration=True)

_sky130_subtask = {
    "name": "sky130",
    "doc": "Generate Sky130 PDK salt package",
    "file_dep": _deps,
    "targets": (
        _sky130_saltpythondir,
        _sky130_grainfile,
        _sky130_readmefile,
    ),
    "actions": (
        _sky130,
    )
}


# C4M/PDK/IHPSG13G2 salt package subtask


_ihpsg13g2_saltdir = _salt_submoddir.joinpath("C4M_PDK_IHPSG13G2")
_ihpsg13g2_saltpythondir = _ihpsg13g2_saltdir.joinpath("python")
_ihpsg13g2_grainfile = _ihpsg13g2_saltdir.joinpath("grain.xml")
_ihpsg13g2_saltdocdir = _ihpsg13g2_saltdir.joinpath("doc")
_ihpsg13g2_readmefile = _ihpsg13g2_saltdocdir.joinpath("readme.html")
_ihpsg13g2_saltmacrodir = _ihpsg13g2_saltdir.joinpath("pymacros")
_ihpsg13g2_macrofile = _ihpsg13g2_saltmacrodir.joinpath("RegisterPrimLib.lym")
_ihpsg13g2_salttechdir = _ihpsg13g2_saltdir.joinpath("tech")
_deps = (
    __file__, version_file,
    *ihpsg13g2_moddir.joinpath("deps", "pdkmaster-io-klayout", "c4m").rglob("*.py"),
    *ihpsg13g2_moddir.joinpath("c4m").rglob("*.py"),
)
def _ihpsg13g2():
    ret = os.system(f"cd {ihpsg13g2_moddir}; pdm doit klayout")
    assert ret == 0

    if _ihpsg13g2_saltpythondir.exists():
        rmtree(_ihpsg13g2_saltpythondir)
    if _ihpsg13g2_saltdocdir.exists():
        rmtree(_ihpsg13g2_saltdocdir)
    if _ihpsg13g2_saltmacrodir.exists():
        rmtree(_ihpsg13g2_saltmacrodir)
    if _ihpsg13g2_salttechdir.exists():
        rmtree(_ihpsg13g2_salttechdir)

    write_saltreadme(salt_dir=_ihpsg13g2_saltdir)

    copytree(
        ihpsg13g2_moddir.joinpath("c4m", "pdk", "ihpsg13g2"),
        _ihpsg13g2_saltpythondir.joinpath("c4m", "pdk", "ihpsg13g2"),
    )
    for d in _ihpsg13g2_saltpythondir.joinpath("c4m").rglob("__pycache__"):
        rmtree(d)
    copytree(
        ihpsg13g2_moddir.joinpath("open_pdk", "C4M.IHPSG13G2", "libs.tech", "klayout", "tech", "C4M.IHPSG13G2"),
        _ihpsg13g2_salttechdir,
    )

    grain_tree = _SaltGrainTree(
        name="C4M/PDK/IHPSG13G2", title="The Chips4Makers PDKMaster based PDK for IHP SG13G2",
        doc=dedent("""
            This is Chips4Makers PDK version of the IHP SG13G2 PDK using the project Arrakeen framework.
        """[1:]),
        deps=(
            ("PDKMaster", _version),
            ("C4M/FlexLibs", _version),
        ),
    )
    grain_tree.write(_ihpsg13g2_grainfile, encoding="unicode", xml_declaration=True)

    create_folder(_ihpsg13g2_saltdocdir)
    readme_tree = _READMETree(
        header="Chips4Makers IHP SG13G2 PDK",
        desc=dedent("""
            This KLayout package contains the Chips4Makers PDK version of the IHP SG13G2 PDK using the project Arrakeen
            framework.
        """),
    )
    readme_tree.write(_ihpsg13g2_readmefile, encoding="unicode", method="html")

    create_folder(_ihpsg13g2_saltmacrodir)
    macro_tree = _PrimLibMacroTree(techname="C4M.IHPSG13G2", modname="ihpsg13g2")
    macro_tree.write(_ihpsg13g2_macrofile, encoding="unicode", xml_declaration=True)

_ihpsg13g2_subtask = {
    "name": "IHPSG13G2",
    "doc": "Generate IHP sg13g2 PDK salt package",
    "file_dep": _deps,
    "targets": (
        _ihpsg13g2_saltpythondir,
        _ihpsg13g2_grainfile,
        _ihpsg13g2_readmefile,
    ),
    "actions": (
        _ihpsg13g2,
    )
}


# C4M/PDK/GF180MCU salt package subtask


_gf180mcu_saltdir = _salt_submoddir.joinpath("C4M_PDK_GF180MCU")
_gf180mcu_saltpythondir = _gf180mcu_saltdir.joinpath("python")
_gf180mcu_grainfile = _gf180mcu_saltdir.joinpath("grain.xml")
_gf180mcu_saltpythondir = _gf180mcu_saltdir.joinpath("python")
_gf180mcu_saltdocdir = _gf180mcu_saltdir.joinpath("doc")
_gf180mcu_readmefile = _gf180mcu_saltdocdir.joinpath("readme.html")
_gf180mcu_saltmacrodir = _gf180mcu_saltdir.joinpath("pymacros")
_gf180mcu_macrofile = _gf180mcu_saltmacrodir.joinpath("RegisterPrimLib.lym")
_gf180mcu_salttechdir = _gf180mcu_saltdir.joinpath("tech")
_deps = (
    __file__, version_file,
    *gf180mcu_moddir.joinpath("deps", "pdkmaster-io-klayout", "c4m").rglob("*.py"),
    *gf180mcu_moddir.joinpath("c4m").rglob("*.py"),
)
def _gf180mcu():
    ret = os.system(f"cd {gf180mcu_moddir}; pdm doit klayout")
    assert ret == 0

    if _gf180mcu_saltpythondir.exists():
        rmtree(_gf180mcu_saltpythondir)
    if _gf180mcu_saltdocdir.exists():
        rmtree(_gf180mcu_saltdocdir)
    if _gf180mcu_saltmacrodir.exists():
        rmtree(_gf180mcu_saltmacrodir)
    if _gf180mcu_salttechdir.exists():
        rmtree(_gf180mcu_salttechdir)

    write_saltreadme(salt_dir=_gf180mcu_saltdir)

    copytree(
        gf180mcu_moddir.joinpath("c4m", "pdk", "gf180mcu"),
        _gf180mcu_saltpythondir.joinpath("c4m", "pdk", "gf180mcu"),
    )
    for d in _gf180mcu_saltpythondir.joinpath("c4m").rglob("__pycache__"):
        rmtree(d)
    copytree(
        gf180mcu_moddir.joinpath("open_pdk", "C4M.GF180MCU", "libs.tech", "klayout", "tech", "C4M.GF180MCU"),
        _gf180mcu_salttechdir,
    )

    grain_tree = _SaltGrainTree(
        name="C4M/PDK/GF180MCU", title="The Chips4Makers PDKMaster based PDK for GF 180mcu technology",
        doc=dedent("""
            This is Chips4Makers PDK version of the GF 180mcu PDK using the project Arrakeen framework.
        """[1:]),
        deps=(
            ("PDKMaster", _version),
            ("C4M/FlexLibs", _version),
        ),
    )
    grain_tree.write(_gf180mcu_grainfile, encoding="unicode", xml_declaration=True)

    create_folder(_gf180mcu_saltdocdir)
    readme_tree = _READMETree(
        header="Chips4Makers GF 180mcu PDK",
        desc=dedent("""
            This KLayout package contains the Chips4Makers PDK version of the GF 180mcu PDK using the project Arrakeen
            framework.
        """),
    )
    readme_tree.write(_gf180mcu_readmefile, encoding="unicode", method="html")

    create_folder(_gf180mcu_saltmacrodir)
    macro_tree = _PrimLibMacroTree(techname="C4M.GF180MCU", modname="gf180mcu")
    macro_tree.write(_gf180mcu_macrofile, encoding="unicode", xml_declaration=True)

_gf180mcu_subtask = {
    "name": "GF180MCU",
    "doc": "Generate GF 180MCU PDK salt package",
    "file_dep": _deps,
    "targets": (
        _gf180mcu_saltpythondir,
        _gf180mcu_grainfile,
        _gf180mcu_readmefile,
    ),
    "actions": (
        _gf180mcu,
    )
}


# pydoit salt task


def task_salt():
    """Generate KLayout salt packages"""
    yield from (
        _pdkmaster_subtask, _flexlibs_subtask,
        _sky130_subtask, _ihpsg13g2_subtask, _gf180mcu_subtask,
    )


def task_copy_salt():
    """Copy klayout salt packages to .klayout in home directory.
    This overwrites existing salts Chips4Makers salts there, It is meant to test new
    salt packages locally before committing it.
    """
    _klayout_homesaltdir = Path.home().joinpath(".klayout", "salt")
    _c4m_homesaltdir = _klayout_homesaltdir.joinpath("C4M")
    _pdk_homesaltdir = _c4m_homesaltdir.joinpath("PDK")

    _pdkmaster_homesaltdir = _klayout_homesaltdir.joinpath("PDKMaster")
    _flexlibs_homesaltdir = _c4m_homesaltdir.joinpath("FlexLibs")
    _sky130_homesaltdir = _pdk_homesaltdir.joinpath("Sky130")
    _ihpsg13g2_homesaltdir = _pdk_homesaltdir.joinpath("IHPSG13G2")
    _gf180mcu_homesaltdir = _pdk_homesaltdir.joinpath("GF180MCU")

    def do():
        rmtree(_pdkmaster_homesaltdir, ignore_errors=True)
        rmtree(_c4m_homesaltdir, ignore_errors=True)

        create_folder(_pdk_homesaltdir)

        ign = ignore_patterns(".git")
        copytree(_pdkmaster_saltdir, _pdkmaster_homesaltdir, ignore=ign)
        copytree(_flexlibs_saltdir, _flexlibs_homesaltdir, ignore=ign)
        copytree(_sky130_saltdir, _sky130_homesaltdir, ignore=ign)
        copytree(_ihpsg13g2_saltdir, _ihpsg13g2_homesaltdir, ignore=ign)
        copytree(_gf180mcu_saltdir, _gf180mcu_homesaltdir, ignore=ign)
    
    return {
        "task_dep": (
            "salt",
        ),
        "targets": (
            _pdkmaster_homesaltdir,
            _flexlibs_homesaltdir,
            _sky130_homesaltdir,
            _ihpsg13g2_homesaltdir,
            _gf180mcu_homesaltdir,
        ),
        "actions": (
            do,
        ),
    }

def task_reset_salt():
    """Reset Salt git submodules to git tracked commit"""

    return {
        "actions": (
            "cd Salt/PDKMaster; git reset --hard",
            "cd Salt/C4M_FlexLibs; git reset --hard",
            "cd Salt/C4M_PDK_Sky130; git reset --hard",
            "cd Salt/C4M_PDK_IHPSG13G2; git reset --hard",
            "cd Salt/C4M_PDK_GF180MCU; git reset --hard",
            "git submodule update Salt",
        ),
    }