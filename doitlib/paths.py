# SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
from pathlib import Path


top_dir = Path(__file__).parent.parent

pdkmaster_dir = top_dir.joinpath("PDKMaster")
pdkmaster_moddir = pdkmaster_dir.joinpath("PDKMaster")
io_klayout_moddir = pdkmaster_dir.joinpath("pdkmaster-io-klayout")

flexlibs_dir = top_dir.joinpath("flexlibs")
flexcell_moddir = flexlibs_dir.joinpath("c4m-flexcell")
flexio_moddir = flexlibs_dir.joinpath("c4m-flexio")

pdks_dir = top_dir.joinpath("PDKs")
sky130_moddir = pdks_dir.joinpath("c4m-pdk-sky130")
gf180mcu_moddir = pdks_dir.joinpath("c4m-pdk-gf180mcu")
ihpsg13g2_moddir = pdks_dir.joinpath("c4m-pdk-ihpsg13g2")
