#!/bin/env python3
# SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
import os
from pathlib import Path
from typing import Optional


__all__ = ["version_file", "get_version"]


version_file = Path(__file__).parents[1].joinpath("version.txt")
version: Optional[str] = None
def get_version():
    global version

    if version is None:
        with version_file.open("r") as f:
            version = next(iter(f))[:-1]

        try:
            idx = version.index("+")
        except:
            pass
        else:
            version = version[:idx]

    return version

if __name__ == "__main__":
    print(get_version())
