The code in this repository is multi-licensed. It can be distributed under any of the following licenses:

1. [GNU Aferro General Public License V3.0](LICENSES/agpl-3.0.txt) or any later version (AGPL-3.0-or-later)
1. [GNU General Public License V2.0](LICENSES/gpl-2.0.txt) or any later version (GPL-2.0-or-later)
1. [CERN Open Hardware Licence Version 2 - Strongly Reciprocal](LICENSES/cern_ohl_s_v2.txt) (CERN-OHL-S-2.0) or any later version of the license.
1. [Apache License Version 2.0](LICENSES/apache-2.0.txt)

In the future, the license list may be reduced. Files fully generated as output of running this code are
not considered derivative works of this code and are not bound by this license. The user who runs the
code can decide on the license of these generated files.
