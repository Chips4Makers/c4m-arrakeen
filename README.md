# Project Arrakeen

This is an umbrella project providing a framework for portable PDKs and scalable digital and analog building blocks.
The setup of this project is currently in exploration phase to find out right setup to aid development, dependency management, user friendliness etc.

# Guidelines

Project Arrakeen has a set of guidelines common to the main project and all subprojects. These are:

* [Contributing.md](Contributing.md): contribution guide lines
* [LICENSE.md](LICENSE.md): license of release code
* [LICENSE_rationale.md](LICENSE_rationale.md): rationale for used multi-license
